using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class GridManager : MonoBehaviour, IResetAble
{
    [SerializeField] private GameObject gridTileGameObject;
    [SerializeField] private Vector3 startingPosition;
    [SerializeField] private Vector3 gridOffset;
    private readonly Tuple<int, int> gridTileAmount = new Tuple<int, int>(3, 3);
    private float gridOffSetIncrementX;
    private float gridOffSetIncrementY;
    private List<GridTile> gridTiles = new List<GridTile>();
    private Queue<GridTile> queueGridTiles = new Queue<GridTile>();
    public event Action RandomTilesReady;
    public event Action ChoosingCorrectAnswer;
    public event Action QueueEmpty;

    private void Awake()
    {
        PopulateGridTile();
       
    }


    private void PopulateGridTile()
    {
        gridOffSetIncrementX = 0f;
        gridOffSetIncrementY = 0f;
        for (int i = 0; i < gridTileAmount.Item1; i++)
        {
            for (int j = 0; j < gridTileAmount.Item2; j++)
            {
                GameObject gridGameObject = Instantiate(gridTileGameObject, transform);
                Vector3 startingPositionIncrement = new Vector3(startingPosition.x + gridOffSetIncrementX,
                    startingPosition.y + gridOffSetIncrementY, 0f);
                gridGameObject.transform.position = startingPositionIncrement;
                GridTile gridTile = gridGameObject.GetComponent<GridTile>();
                gridTile.GridTileID = startingPositionIncrement.x + "," + startingPositionIncrement.y;
                gridTiles.Add(gridTile);
                gridOffSetIncrementX += gridOffset.x;
            }

            gridOffSetIncrementX = 0f;
            gridOffSetIncrementY += gridOffset.x;
        }
    }


    public IEnumerator PopulateRandomGridTile(int randomGridTileAmount, float showTime)
    {
        Renderer curGridTileRenderer;
        
        for (int i = 0; i < randomGridTileAmount; i++)
        {
            curGridTileRenderer = null;
            int randomNumber = Random.Range(0, gridTiles.Count);
            curGridTileRenderer = gridTiles[randomNumber].GetComponent<Renderer>();
            curGridTileRenderer.material.color = Color.black;
            queueGridTiles.Enqueue(gridTiles[randomNumber]);
            yield return new WaitForSeconds(showTime);


            curGridTileRenderer.material.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }

        RandomTilesReady?.Invoke();
    }

    public void MakeAllGridTilesElectable()
    {
        foreach (GridTile gridTile in gridTiles)
        {
            gridTile.canBeChosen = true;
        }
    }

    public void MakeAllGridTilesNotElectable()
    {
        foreach (GridTile gridTile in gridTiles)
        {
            gridTile.canBeChosen = false;
        }
    }

    private void ClearQueueRandomGridTiles()
    {
        queueGridTiles.Clear();
    }

    public void CheckCurrentRandomTileInQueue(GridTile currentGridTile)
    {
        if (currentGridTile.Equals(queueGridTiles.Dequeue()))
        {
            ChoosingCorrectAnswer?.Invoke();
        }

        if (queueGridTiles.Count == 0)
        {
            QueueEmpty?.Invoke();
        }
    }

    public void ResetObject()
    {
        ClearQueueRandomGridTiles();
    }
}