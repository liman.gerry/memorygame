using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour,IResetAble
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private GameObject resetButtonUI;

    private ScoreManager scoreManager;
    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        scoreManager = FindObjectOfType<ScoreManager>();

       UpdateScoreUI(0);

        scoreManager.ScoreChanged += OnScoreChanged;
        gameManager.GameOver += OnGameOver;
    }

    private void OnGameOver()
    {
        resetButtonUI.SetActive(true);
    }

    private void OnScoreChanged(int score)
    {
        UpdateScoreUI(score);
    }

    private void UpdateScoreUI(int score)
    {
        scoreText.text = "Score : " + score.ToString();
    }


    public void ResetObject()
    {
        resetButtonUI.SetActive(false);
    }
}