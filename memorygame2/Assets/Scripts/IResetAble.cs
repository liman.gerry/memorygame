public interface IResetAble
{
    void ResetObject();
}