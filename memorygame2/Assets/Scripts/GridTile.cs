using UnityEngine;

public class GridTile : MonoBehaviour
{
    public string GridTileID { get; set; }
    public bool canBeChosen;
    private GridManager gridManager;

    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();
    }

    private void OnMouseDown()
    {
        if (canBeChosen)
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    private void OnMouseUp()
    {
        if (canBeChosen)
        {
            GetComponent<Renderer>().material.color = Color.white;
            gridManager.CheckCurrentRandomTileInQueue(this);
        }
    }

    
}