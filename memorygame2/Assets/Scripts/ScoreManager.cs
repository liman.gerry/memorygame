using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour, IResetAble
{
    [field: SerializeField] private int Score { set; get; }

    private GridManager gridManager;

    public event Action<int> ScoreChanged;

    // Start is called before the first frame update
    void Start()
    {
        ResetScore();
        gridManager = FindObjectOfType<GridManager>();
        gridManager.ChoosingCorrectAnswer += OnChoosingCorrectAnswer;
    }

    private void OnChoosingCorrectAnswer()
    {
        Score += 1;
        ScoreChanged?.Invoke(Score);
    }

    public void ResetScore()
    {
        Score = 0;
    }

    public void ResetObject()
    {
        Score = 0;
        ScoreChanged?.Invoke(Score);
    }
}