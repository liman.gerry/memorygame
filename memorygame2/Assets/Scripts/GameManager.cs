using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class GameManager : MonoBehaviour,IResetAble
{
    [SerializeField] private float showTime = 0.5f;
    //private bool playerCanChoose = false;
    private GridManager gridManager;
    public bool isGameOver;

    public event Action GameOver;
    
    
    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();
    }
    
    void Start()
    {
        gridManager.RandomTilesReady += OnRandomTilesReady;
        PrepareGame();
        gridManager.QueueEmpty += OnQueueEmpty;
    }

    private void OnQueueEmpty()
    {
        gridManager.MakeAllGridTilesNotElectable();
        isGameOver = true;
        GameOver?.Invoke();
    }
    
    private void PrepareGame()
    {
        StartCoroutine(gridManager.PopulateRandomGridTile(Random.Range(1, 10), showTime));
    }
    private void OnRandomTilesReady()
    {
        gridManager.MakeAllGridTilesElectable();
    }


    public void ResetObject()
    {
        PrepareGame();
    }
}